#include <assert.h>
#include "isl_map_ppl.h"

int main()
{
	isl_ctx *ctx;
	isl_map *map1, *map2;
	ppl_Pointset_Powerset_C_Polyhedron_t ps;

	ppl_initialize();
	ctx = isl_ctx_alloc();

	map1 = isl_map_read_from_str(ctx,
		"[n] -> { [x] -> [y,z] : exists (e : y = x + 1 and "
			"0 <= x and x <= n and y + x = 3 e) or "
			"exists (e2 : z = x and x = 10 e2) }", -1);

	ps = isl_map_to_ppl(map1);
	assert(ps);

	map2 = isl_map_new_from_ppl(ps, isl_map_get_dim(map1));
	assert(map2);

	assert(isl_map_is_equal(map1, map2));

	ppl_delete_Pointset_Powerset_C_Polyhedron(ps);
	isl_map_free(map1);
	isl_map_free(map2);

	isl_ctx_free(ctx);
	ppl_finalize();

	return 0;
}
