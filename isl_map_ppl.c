#include "isl_constraint.h"
#include "isl_dim.h"
#include "isl_set_ppl.h"

static void ppl_dim_to_isl_dim(isl_basic_map *bmap, int pos,
				enum isl_dim_type *type, int *i_pos)
{
	if (pos < isl_basic_map_dim(bmap, isl_dim_out)) {
		*type = isl_dim_out;
		*i_pos = pos;
		return;
	}
	pos -= isl_basic_map_dim(bmap, isl_dim_out);

	if (pos < isl_basic_map_dim(bmap, isl_dim_in)) {
		*type = isl_dim_in;
		*i_pos = pos;
		return;
	}
	pos -= isl_basic_map_dim(bmap, isl_dim_in);

	if (pos < isl_basic_map_dim(bmap, isl_dim_div)) {
		*type = isl_dim_div;
		*i_pos = pos;
		return;
	}
	pos -= isl_basic_map_dim(bmap, isl_dim_div);

	*type = isl_dim_param;
	*i_pos = pos;
}

ppl_Polyhedron_t isl_basic_map_to_ppl(__isl_keep isl_basic_map *bmap)
{
	unsigned dim;
	ppl_Polyhedron_t pol;
	isl_constraint *c;
	isl_dim *dims = NULL;
	ppl_Coefficient_t coeff;
	int allocated_coeff = 0;
	isl_int v;

	if (!bmap)
		return NULL;

	dim = isl_basic_map_dim(bmap, isl_dim_all);
	if (ppl_new_C_Polyhedron_from_space_dimension(&pol, dim, 0) < 0)
		return NULL;

	isl_int_init(v);

	if (ppl_new_Coefficient(&coeff) < 0)
		goto error;
	allocated_coeff = 1;

	bmap = isl_basic_map_copy(bmap);
	for (c = isl_basic_map_first_constraint(bmap); c;
	     c = isl_constraint_next(c)) {
		int i;
		int err = 0;
		ppl_Linear_Expression_t le;
		ppl_Constraint_t con;
		int eq = isl_constraint_is_equality(c);

		if (ppl_new_Linear_Expression_with_dimension(&le, dim) < 0)
			goto error;

		for (i = 0; i < dim; ++i) {
			int j;
			enum isl_dim_type type;

			ppl_dim_to_isl_dim(bmap, i, &type, &j);
			isl_constraint_get_coefficient(c, type, j, &v);
			ppl_assign_Coefficient_from_mpz_t(coeff, v);

			if (ppl_Linear_Expression_add_to_coefficient(le, i, coeff) < 0)
				break;
		}
		if (i < dim)
			err = 1;

		isl_constraint_get_constant(c, &v);
		ppl_assign_Coefficient_from_mpz_t(coeff, v);
		if (ppl_Linear_Expression_add_to_inhomogeneous(le, coeff) < 0)
			err = 1;

		if (ppl_new_Constraint(&con, le,
				eq ? PPL_CONSTRAINT_TYPE_EQUAL :
				     PPL_CONSTRAINT_TYPE_GREATER_OR_EQUAL) < 0)
			err = 1;

		ppl_delete_Linear_Expression(le);

		if (!err && ppl_Polyhedron_add_constraint(pol, con) < 0)
			err = 1;

		ppl_delete_Constraint(con);
		if (err)
			goto error;
	}

	ppl_delete_Coefficient(coeff);
	isl_int_clear(v);

	return pol;
error:
	isl_int_clear(v);
	if (allocated_coeff)
		ppl_delete_Coefficient(coeff);
	isl_dim_free(dims);
	ppl_delete_Polyhedron(pol);
	return NULL;
}

static add_basic_map(isl_basic_map *bmap, void *user)
{
	ppl_Polyhedron_t pol;
	ppl_Pointset_Powerset_C_Polyhedron_t ps;

	ps = (ppl_Pointset_Powerset_C_Polyhedron_t) user;

	pol = isl_basic_map_to_ppl(bmap);
	isl_basic_map_free(bmap);

	if (!pol)
		return -1;

	if (ppl_Pointset_Powerset_C_Polyhedron_add_disjunct(ps, pol))
		goto error;

	ppl_delete_Polyhedron(pol);
	return 0;
error:
	ppl_delete_Polyhedron(pol);
	return -1;
}

static get_total_dim(isl_basic_map *bmap, void *user)
{
	unsigned *dim = (unsigned *)user;

	*dim = isl_basic_map_total_dim(bmap);
	isl_basic_map_free(bmap);

	return 0;
}

ppl_Pointset_Powerset_C_Polyhedron_t isl_map_to_ppl(__isl_keep isl_map *map)
{
	unsigned dim;
	ppl_Pointset_Powerset_C_Polyhedron_t ps;

	map = isl_map_align_divs(map);
	if (!map)
		return NULL;

	if (isl_map_foreach_basic_map(map, &get_total_dim, &dim) < 0)
		return NULL;

	if (ppl_new_Pointset_Powerset_C_Polyhedron_from_space_dimension(&ps, dim, 1) < 0)
		return NULL;

	if (isl_map_foreach_basic_map(map, &add_basic_map, ps) < 0)
		goto error;

	return ps;
error:
	ppl_delete_Pointset_Powerset_C_Polyhedron(ps);
	return NULL;
}

__isl_give isl_basic_map *isl_basic_map_new_from_ppl(
	ppl_const_Polyhedron_t pol, __isl_take isl_dim *dim)
{
	unsigned d;
	isl_int v;
	ppl_Coefficient_t coeff;
	ppl_const_Constraint_System_t cs;
	ppl_Constraint_System_const_iterator_t cit, cit_end;
	isl_basic_map *bmap = NULL;
	ppl_dimension_type ppl_dim;
	int extra;

	isl_int_init(v);

	if (ppl_Polyhedron_get_minimized_constraints(pol, &cs) < 0)
		goto error;
	if (ppl_Constraint_System_space_dimension(cs, &ppl_dim) < 0)
		goto error;

	if (ppl_new_Constraint_System_const_iterator(&cit) < 0)
		goto error;
	if (ppl_new_Constraint_System_const_iterator(&cit_end) < 0)
		goto error1;
	if (ppl_Constraint_System_begin(cs, cit) < 0)
		goto error2;
	if (ppl_Constraint_System_end(cs, cit_end) < 0)
		goto error2;

	if (ppl_new_Coefficient(&coeff) < 0)
		goto error2;

	extra = ppl_dim - isl_dim_total(dim);
	dim = isl_dim_add(dim, isl_dim_in, extra);
	bmap = isl_basic_map_universe(isl_dim_copy(dim));

	d = isl_dim_total(dim);

	while (!ppl_Constraint_System_const_iterator_equal_test(cit, cit_end)) {
		struct isl_constraint *c;
		ppl_const_Constraint_t con;
		int i;
		int eq;

		if (ppl_Constraint_System_const_iterator_dereference(cit, &con) < 0)
			goto error3;

		eq = ppl_Constraint_type(con) == PPL_CONSTRAINT_TYPE_EQUAL;
		if (eq)
			c = isl_equality_alloc(isl_dim_copy(dim));
		else
			c = isl_inequality_alloc(isl_dim_copy(dim));

		for (i = 0; i < d; ++i) {
			int j;
			enum isl_dim_type type;

			ppl_dim_to_isl_dim(bmap, i, &type, &j);
			if (ppl_Constraint_coefficient(con, i, coeff) < 0)
				goto error3;
			if (ppl_Coefficient_to_mpz_t(coeff, v) < 0)
				goto error3;
			isl_constraint_set_coefficient(c, type, j, v);
		}
		if (ppl_Constraint_inhomogeneous_term(con, coeff) < 0)
			goto error3;
		if (ppl_Coefficient_to_mpz_t(coeff, v) < 0)
			goto error3;
		isl_constraint_set_constant(c, v);

		bmap = isl_basic_map_add_constraint(bmap, c);

		if (ppl_Constraint_System_const_iterator_increment(cit) < 0)
			goto error3;
	}

	ppl_delete_Coefficient(coeff);
	ppl_delete_Constraint_System_const_iterator(cit_end);
	ppl_delete_Constraint_System_const_iterator(cit);

	isl_int_clear(v);

	isl_dim_free(dim);

	bmap = isl_basic_map_project_out(bmap, isl_dim_in,
			isl_basic_map_dim(bmap, isl_dim_in) - extra, extra);

	return bmap;
error3:
	ppl_delete_Coefficient(coeff);
error2:
	ppl_delete_Constraint_System_const_iterator(cit_end);
error1:
	ppl_delete_Constraint_System_const_iterator(cit);
error:
	isl_int_clear(v);
	isl_dim_free(dim);
	isl_basic_map_free(bmap);
	return NULL;
}

__isl_give isl_map *isl_map_new_from_ppl(
	ppl_const_Pointset_Powerset_C_Polyhedron_t ps, __isl_take isl_dim *dim)
{
	isl_map *map;
	ppl_Pointset_Powerset_C_Polyhedron_const_iterator_t pit, pit_end;

	map = isl_map_empty(isl_dim_copy(dim));
	if (!map)
		goto error;

	if (ppl_new_Pointset_Powerset_C_Polyhedron_const_iterator(&pit) < 0)
		goto error;
	if (ppl_Pointset_Powerset_C_Polyhedron_const_iterator_begin(ps, pit) < 0)
		goto error1;
	if (ppl_new_Pointset_Powerset_C_Polyhedron_const_iterator(&pit_end) < 0)
		goto error1;
	if (ppl_Pointset_Powerset_C_Polyhedron_const_iterator_end(ps, pit_end) < 0)
		goto error2;

	while (!ppl_Pointset_Powerset_C_Polyhedron_const_iterator_equal_test(pit, pit_end)) {
		ppl_const_Polyhedron_t pol;
		isl_basic_map *bmap;

		if (ppl_Pointset_Powerset_C_Polyhedron_const_iterator_dereference(pit, &pol) < 0)
			goto error2;

		bmap = isl_basic_map_new_from_ppl(pol, isl_dim_copy(dim));

		map = isl_map_union(map, isl_map_from_basic_map(bmap));

		if (ppl_Pointset_Powerset_C_Polyhedron_const_iterator_increment(pit) < 0)
			goto error2;
	}

	ppl_delete_Pointset_Powerset_C_Polyhedron_const_iterator(pit);
	ppl_delete_Pointset_Powerset_C_Polyhedron_const_iterator(pit_end);

	isl_dim_free(dim);

	return map;
error2:
	ppl_delete_Pointset_Powerset_C_Polyhedron_const_iterator(pit_end);
error1:
	ppl_delete_Pointset_Powerset_C_Polyhedron_const_iterator(pit);
error:
	isl_map_free(map);
	isl_dim_free(dim);
	return NULL;
}

ppl_Pointset_Powerset_C_Polyhedron_t isl_set_to_ppl(__isl_keep isl_set *set)
{
	return isl_map_to_ppl((isl_map *)set);
}

__isl_give isl_set *isl_set_new_from_ppl(
	ppl_const_Pointset_Powerset_C_Polyhedron_t ps, __isl_take isl_dim *dim)
{
	return (isl_set *)isl_map_new_from_ppl(ps, dim);
}
