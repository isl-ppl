#ifndef ISL_MAP_PPL_H
#define ISL_MAP_PPL_H

#include <isl_map.h>
#include <isl_ppl.h>

__isl_give isl_map *isl_map_new_from_ppl(
	ppl_const_Pointset_Powerset_C_Polyhedron_t ps, __isl_take isl_dim *dim);
ppl_Pointset_Powerset_C_Polyhedron_t isl_map_to_ppl(__isl_keep isl_map *map);

#endif
