#ifndef ISL_SET_PPL_H
#define ISL_SET_PPL_H

#include <isl_set.h>
#include <isl_ppl.h>

__isl_give isl_set *isl_set_new_from_ppl(
	ppl_const_Pointset_Powerset_C_Polyhedron_t ps, __isl_take isl_dim *dim);
ppl_Pointset_Powerset_C_Polyhedron_t isl_set_to_ppl(__isl_keep isl_set *set);

#endif
